/***
===========================
Main JS functionality
===========================
***/

$(document).ready(function(){
    var viewportWidth = $(document).width();

    $('.slick-slider').slick();

    // show .navmenu close and open icons
    $(".navmenu__icon").click(function(){
        $(".navmenu__icon-open").toggle();
        $(".navmenu__icon-close").toggle();
        $(".navmenu__container").toggle(
            0,
            function(){
                if(viewportWidth < 1024 && viewportWidth > 767 && $(this).css("display") == "block")
                    $(this).addClass("flexme");
                else
                    $(this).removeClass("flexme");
        });
    });

    // show/hide .navsubmenu on toggle
    $(".navmenu__item").click(function(){
        $($(this) + ".navsubmenu").toggle();
    });

    $(".page-header__search-button").click(function(event){
        event.preventDefault();
    });


    if(viewportWidth < 1024) {
        $(".page-header__search-button").click(function(){
            //$(".page-header__search-input").animate({width: "toggle"}, 0);
            $(".page-header__search-form").css("borderColor", "#986945");
            $(".page-header__search-input").toggle(0);
            if($(".page-header__search-input").css("display") == "none")
                $(".page-header__search-form").css("borderColor", "transparent");
            $(".page-header__search-input").focus();
        });
    }
    if(viewportWidth < 768){
        $(".page-header__search-button").click(function(){
            $(".page-header--company-logo").toggle(0);
        });
    }




});